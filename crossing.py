from cbp import simple_get
from bs4 import BeautifulSoup
import re
import datetime
import time
from sqlLiteActions import Bdb
import os





class Crossing:
    def __init__(self):
        self.location = "none"
        self.mainCrossing = "none"
        self.maxLanes = "N/A"
        self.standardLane = {}
        self.standardLane['time'] = datetime.datetime(2000,1,1,0,0).strftime("%Y-%m-%d %H:%M:%S")
        self.standardLane['delay'] = 0
        self.standardLane['openLanes'] = 0
        self.readyLane = {}
        self.readyLane['time'] = datetime.datetime(2000,1,1,0,0).strftime("%Y-%m-%d %H:%M:%S")
        self.readyLane['delay'] = 0
        self.readyLane['openLanes'] = 0
        self.sentriLane = {}
        self.sentriLane['isOpen'] = False
        self.sentriLane['time'] = datetime.datetime(2000,1,1,0,0).strftime("%Y-%m-%d %H:%M:%S")
        self.sentriLane['delay'] = 0
        self.sentriLane['openLanes'] = 0
        self.crossingType = "none"
        self.fastLane = {}
        self.fastLane['time'] = datetime.datetime(2000,1,1,0,0).strftime("%Y-%m-%d %H:%M:%S")
        self.fastLane['delay'] = 0
        self.fastLane['openLanes'] = 0
        self.mexicanList = ["Andrade" , "Brownsville" , "Calexico" , "Columbus" , "Del Rio" , "Douglas (Raul Hector Castro)" , "Eagle Pass" , "El Paso" , "Fabens" , "Fort Hancock" , "Hidalgo/Pharr" , "Laredo" , "Lukeville" , "Naco" , "Nogales" , "Otay Mesa" , "Presidio" , "Progreso" , "Rio Grande City" , "Roma" , "San Luis" , "San Ysidro" , "Santa Teresa" , "Tecate"]

    def printCrossing(self):
        print("Location: %s" % (self.location))
        print("Crossing Name {}".format(self.mainCrossing))
        print("Crossing Type %s" % (self.crossingType))
        print("Max lanes: %s" % (self.maxLanes))
        print("Standard lane time: %s" % (str(self.standardLane['time'])))
        print("Standard lane delay: %s" % (self.standardLane['delay']))
        print("Standard lanes open: %s" % (self.standardLane['openLanes']))
        print("Ready lanes time: %s" % (str(self.readyLane['time'])))
        if self.crossingType != "Commercial":
            print("Ready lane delay: %s" % (self.readyLane['delay']))
            print("Ready lanes open: %s" % (self.readyLane['openLanes']))
        else:
            print("fastLane delay: %s" % (self.fastLane['delay']))
            print("fastLane open: %s" % (self.fastLane['openLanes']))
            print("fastlane lanes time: %s" % (str(self.fastLane['time'])))
        

        if self.crossingType == "Passenger":
            print("Sentri lanes time: %s" % (str(self.sentriLane['time'])))
            print("Sentri lane delay: %s" % (self.sentriLane['delay']))
            print("Sentri lanes open: %s" % (self.sentriLane['openLanes']))

    
    #This static method returns an array of the main sub crossings at the border crossings. 
    #provide url for get request. 
    #returns an array of HTML <article>
    @staticmethod
    def getMainSubCrossings(link):
        #simple get
        mainSubCrossings = []
        # print('getting main subcrossings')
        rawHtmlText = simple_get(link)
        #assign HTML
        
        rawHtml = BeautifulSoup(rawHtmlText, 'html.parser')

        # rawHtml = BeautifulSoup(open("/Users/jamesmitchell/Developer/web/scraping/brownsville.html"), 'html.parser')

        # with open("output2.html", "w") as file:
        #     file.write(str(rawHtml))
        #parse
        rawCrossings = rawHtml.find_all('article')
        count = 0
        for crossing in rawCrossings:
            mainSubCrossings.append(crossing)
            Crossing.getFullSubCrossingInfo(crossing, rawHtml, link)

        return mainSubCrossings

    
    @staticmethod
    def getFullSubCrossingInfo(articleHtml, fullHtml, link):
        details = {}
        rc = Crossing()
        
        details['title'] = articleHtml.div.header.b.get_text()
        rc.mainCrossing = details['title']
        details["commDetails"] = articleHtml.div.header.next_sibling
        details["passDetails"] = details["commDetails"].next_sibling
        details["pedDetails"] = details["passDetails"].next_sibling
        details["borderNotice"] = details["pedDetails"].next_sibling
        details["location"] = SuperCrossing.findLocation(fullHtml, link)
        if details["location"] not in rc.mexicanList:
            return

        commCrossing = Crossing()
        commCrossing.crossingType = "Commercial"
        passCrossing = Crossing()
        passCrossing.crossingType = "Passenger"
        pedCrossing = Crossing()
        pedCrossing.crossingType = "Pedestrian"

        superC = SuperCrossing(pedCrossing, commCrossing, passCrossing, details['title'])

        

        mainCrossings = [superC.commCrossing, superC.passCrossing, superC.pedCrossing]
        for c in mainCrossings:
            c.mainCrossing = details['title']
            c.location = details["location"]


        Crossing.getLaneAndWaitInfo(details['pedDetails'], superC.pedCrossing)
        Crossing.getLaneAndWaitInfo(details['commDetails'], superC.commCrossing)
        Crossing.getLaneAndWaitInfo(details['passDetails'], superC.passCrossing)
        
        for c in mainCrossings:
            c.logDetails()
        # superC.passCrossing.printCrossing()
        # superC.pedCrossing.printCrossing()
        # superC.commCrossing.printCrossing()
    
    @staticmethod
    def getLaneAndWaitInfo(detailsHtml, rc):
        snippetLength = len(detailsHtml.get_text())
        snippet = detailsHtml.get_text()
        # maxLanes = detailsHtml.i.next_sibling
        if snippetLength < 90 and "Lanes Closed" not in snippet:
            pass
            # print("Passing %s. type: %s with snippetLength:%d" % (rc.mainCrossing, rc.crossingType ,snippetLength))
        else:
            # print("INFO:%dx" % (snippetLength))
            #parse
            if "Sentri" in detailsHtml.get_text():
                rc.sentriLane['isOpen'] = True
            Crossing.parseDetails(detailsHtml, rc)
    
    @staticmethod       
    def testRe(matchObject):
        if matchObject == None:
            # print("Object is null")
            return "None"
        else:
            
            return matchObject.group(0)


    @staticmethod
    def parseDetails(details, cross):
        #problems is 
        boldDetails = details.find_all('b')
        # print(details.get_text())
        # noonCatcher = Crossing.testRe(re.search(r'(?<=Standard Lanes: ).+?(?=(Ready)|(Fast))', details.get_text(), flags=0))
        cross.maxLanes = Crossing.testRe(re.search(r'(?<=Maximum Lanes: ).*(?=Standard)', details.get_text(), flags=0))#.group(0)
        cross.standardLane['time'] = SuperCrossing.timeConverter(Crossing.testRe(re.search(r'(?<=Standard Lanes: At ).+?(?=,)', details.get_text(), flags=0)))
        cross.standardLane['delay'] = Crossing.testRe(re.search(r'(?<=, ).+?(?=,)', details.get_text(), flags=0))
        subRegex = Crossing.testRe(re.search(r'(?<=Standard Lanes: ).+?(?=(Ready)|(Fast))', details.get_text(), flags=0))
        cross.standardLane['openLanes'] = Crossing.testRe(re.search(r'(?<=[^T], ).+?(?= lane)', subRegex, flags=0))
        # print(subRegex)
        if subRegex == "Lanes Closed":
            cross.standardLane['openLanes'] = "closed"
            cross.standardLane['time'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            cross.standardLane['delay'] = "closed"
        # print(cross.mainCrossing)
        # print(cross.standardLane['openLanes'])
        cross.readyLane['time'] = SuperCrossing.timeConverter(Crossing.testRe(re.search(r'(?<=Readylane: At ).+?(?=,)', details.get_text(), flags=0)))
        subRegex = Crossing.testRe(re.search(r'(?<=Readylane: ).+?(?=(Sentri)|$)', details.get_text(), flags=0))
 
        cross.readyLane['delay'] = Crossing.testRe(re.search(r'(?<=[ST|DT], ).+?(?=,)', subRegex, flags=0))
        cross.readyLane['openLanes'] = Crossing.testRe(re.search(r'(?<=[^T], ).+?(?= lane)', subRegex, flags=0))
        # print(cross.mainCrossing, cross.crossingType)
        # print(subRegex)
        if subRegex == "Lanes Closed":
            cross.readyLane['openLanes'] = "closed"
            cross.readyLane['time'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            cross.readyLane['delay'] = "closed"
        if subRegex == "N/A":
            cross.readyLaneNull()
        # print(cross.readyLane['openLanes'])   

        if cross.sentriLane['isOpen'] == True:
            #parse
            subRegex = Crossing.testRe(re.search(r'(?<=Sentri Lanes: ).+?(?= open)', details.get_text(), flags=0))
            secondSub = Crossing.testRe(re.search(r'(?<=Sentri Lanes: ).+?(?=$)', details.get_text(), flags=0))
            
            cross.sentriLane["delay"] = Crossing.testRe(re.search(r'(?<=[ST|DT], ).+?(?=,)', subRegex, flags=0))
            cross.sentriLane["openLanes"] = Crossing.testRe(re.search(r'(?<=[^T], ).+?(?= lane)', subRegex, flags=0))
            cross.sentriLane['time'] = SuperCrossing.timeConverter(Crossing.testRe(re.search(r'(?<=Sentri Lanes: At ).+?(?=,)', details.get_text(), flags=0)))
            
            subRegex = Crossing.testRe(re.search(r'(?<=Sentri Lanes: ).+?(?=$)', details.get_text(), flags=0))

            # print(subRegex)
            if subRegex == "Lanes Closed":
                cross.sentriLane['openLanes'] = "closed"
                cross.sentriLane['time'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                cross.sentriLane['delay'] = "closed"
            
            if secondSub == "N/A":
                # print("The second sub is n/a")
                cross.sentriLaneNull()
            
        if cross.crossingType == "Commercial":
            subRegex = Crossing.testRe(re.search(r'(?<=Fast Lanes: ).+?(?=(openSentri)|$)', details.get_text(), flags=0))
            secondSub = Crossing.testRe(re.search(r'(?<=Fast Lanes: ).+?(?=$)', details.get_text(), flags=0))
            cross.fastLane['delay'] = Crossing.testRe(re.search(r'(?<=[ST|DT], ).+?(?=,)', subRegex, flags=0))
            cross.fastLane['openLanes'] = Crossing.testRe(re.search(r'(?<=[^T], ).+?(?= lane)', subRegex, flags=0))
            cross.fastLane['time'] = SuperCrossing.timeConverter(Crossing.testRe(re.search(r'(?<=Fast Lanes: At ).+?(?=,)', details.get_text(), flags=0)))
            
            if secondSub == "Lanes Closed":
                cross.fastLane['openLanes'] = "closed"
                cross.fastLane['time'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                cross.fastLane['delay'] = "closed"

            if secondSub == "N/A":
                # print("The second sub is n/a")
                cross.fastLaneNull()
    
    def adjustForLogging(self):
        self.fixDelays()
        if self.crossingType == "Commercial":
            self.readyLaneNull()
            self.sentriLaneNull()
            pass
                
        if self.crossingType == "Passenger":
            self.fastLaneNull()
            pass

        if self.crossingType == "Pedestrian":
            self.fastLaneNull()
            self.sentriLaneNull()
            pass
    
    def readyLaneNull(self):
        self.readyLane["time"] = "NULL"
        self.readyLane["openLanes"] = "NULL"
        self.readyLane["delay"] = "NULL"
    
    def sentriLaneNull(self):
        self.sentriLane["time"] = "NULL"
        self.sentriLane["openLanes"] = "NULL"
        self.sentriLane["delay"] = "NULL"

    def fastLaneNull(self):
        self.fastLane["time"] = "NULL"
        self.fastLane["openLanes"] = "NULL"
        self.fastLane["delay"] = "NULL"
    
    def fixDelays(self):
        if "min" in str(self.standardLane['delay']):
            newT = Crossing.testRe(re.search(r'.+?(?=min)', self.standardLane['delay'], flags=0))
            # print("%s is now %s" % (self.standardLane['delay'], newT))
            self.standardLane['delay'] = newT
        
        if "min" in str(self.readyLane['delay']):
            newT = Crossing.testRe(re.search(r'.+?(?=min)', self.readyLane['delay'], flags=0))
            # print("%s is now %s" % (self.readyLane['delay'], newT))
            self.readyLane['delay'] = newT

        if "min" in str(self.sentriLane['delay']):
            newT = Crossing.testRe(re.search(r'.+?(?=min)', self.sentriLane['delay'], flags=0))
            # print("%s is now %s" % (self.sentriLane['delay'], newT))
            self.sentriLane['delay'] = newT

        if "min" in str(self.fastLane['delay']):
            newT = Crossing.testRe(re.search(r'.+?(?=min)', self.fastLane['delay'], flags=0))
            # print("%s is now %s" % (self.fastLane['delay'], newT))
            self.fastLane['delay'] = newT


    def logDetails(self):
        dbname = os.environ.get("DATABASENAME", "crossings.db")
        
        tablename = os.environ.get("TABLENAME", "waitlogs")
        logger = Bdb(tablename, dbname)
        self.adjustForLogging()

        logDic = {
            "location": self.location,#string
            "crossingName": self.mainCrossing,
            "maxLanes": self.maxLanes,
            "crossingType": self.crossingType,
            "standardLanes": self.standardLane["openLanes"],
            "standardLaneTime": self.standardLane["time"],
            "standardLaneDelay": self.standardLane["delay"],
            "readyLanes": self.readyLane["openLanes"],
            "readyLanesTime": self.readyLane["time"],
            "readyLanesDelay": self.readyLane["delay"],
            "fastLanes": self.fastLane["openLanes"],
            "fastLanesTime": self.fastLane["time"],
            "fastLanesDelay": self.fastLane["delay"],
            "sentriLanes": self.sentriLane["openLanes"],
            "sentriLanesTime": self.sentriLane["time"],
            "sentriLanesDelay": self.sentriLane["delay"],
            "logTime": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        }

        for key, value in logDic.items():
            if value == "no delay":
                logDic[key] =  0
            # print("key:%s, value:%s" % (key, str(value)))

        if self.maxLanes != "N/A":
            logger.logData(logDic)
            #print("Just logged %s - %s" % (logDic["crossingName"], logDic["crossingType"]))
            logger.conn.close()
            # self.printCrossing()




class SuperCrossing:
    def __init__(self, pedestrian, commercial, passenger, title):
        # print("Making a super class")
        self.pedCrossing = pedestrian
        self.commCrossing = commercial
        self.passCrossing = passenger
        self.title = title
    
    def printSuperCrossing(self):
        pass
        # self.pedCrossing.printCrossing()
        # self.commCrossing.printCrossing()
        # self.passCrossing.printCrossing()
    
    @staticmethod
    def timeConverter(t):
        try:
            # print("time received in converter:%s" % (t))
            cdt = datetime.datetime.now()

            hour = 0
            tz = "AST"
            minutes = 0

            try:
                hour = int(Crossing.testRe(re.search(r'.+?(?=:)', t, flags=0)))
            except:
                pass
            timeOfDay = Crossing.testRe(re.search(r'(?<= ).+?(?= )', t, flags=0))

            if timeOfDay == "pm" and hour < 12:
                hour = hour + 12
            else:
                pass

            try:
                minutes = int(Crossing.testRe(re.search(r'(?<=:)..', t, flags=0)))
            except:
                pass

            day = cdt.day
            month = cdt.month
            year = cdt.year
            if "Noon" in t:
                hour = 12
                minutes = 0
            

            if "idnight" in t:
                hour = 0
                minutes = 0

            forLogDatetime = datetime.datetime(year, month, day, int(hour), minutes)
            return forLogDatetime
        except:
            #print(Exception)
            return datetime.datetime(2000,1,1,0,0)

    @staticmethod
    def findLocation(rawHtml, link):
        # rawHtmlText = simple_get(link)
        location = "none found"
        lastFour = link[-4:]
        allA = rawHtml.find_all('a')
        for a in allA:
            if lastFour in str(a):
                # print(a)
                location = a.get_text().strip()
        return location

