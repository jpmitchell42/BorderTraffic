from sqlLiteActions import Bdb
from crossing import Crossing
from crossing import SuperCrossing



logger = Bdb("waitlogs", "crossings.db")
try:
    logger.deleteTable()
except:
    print("There was no table to delete.")
    pass
logger.setupDatabase()
logger.conn.close()