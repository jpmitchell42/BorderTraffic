import sqlite3
from operator import itemgetter
import os


class Bdb:
    def __init__(self, tablename, dbname):
        self.tablename = tablename
        self.dbname = dbname
        self.conn = sqlite3.connect(dbname)
        self.c = self.conn.cursor()
        os.environ["DATABASENAME"] = dbname
        os.environ["TABLENAME"] = tablename
    
    def deleteTable(self):
        command = "DROP TABLE " + self.tablename
        self.c.execute(command)
        self.conn.commit()
    # @staticmethod
    def setupDatabase(self):
        self.c.execute('''CREATE TABLE '''+ self.tablename + ''' 
        (
            location TEXT,
            crossingName TEXT,
            maxLanes INTEGER,
            crossingType TEXT,
            standardLanes INTEGER,
            standardLaneTime TEXT,
            standardLaneDelay INTEGER,
            readyLanes INTEGER,
            readyLanesTime TEXT,
            readyLanesDelay INTEGER,
            fastLanes INTEGER,
            fastLanesTime TEXT,
            fastLanesDelay INTEGER,
            sentriLanes INTEGER,
            sentriLanesTime TEXT,
            sentriLanesDelay INTEGER,
            logTime TEXT
        )
        ''')

    def printDataBase(self):
        for row in self.c.execute('SELECT * FROM ' + self.tablename):
            print(row)
    def logData(self, dictionaryLog):
        params = [            
                dictionaryLog["location"],
                dictionaryLog["crossingName"],
                dictionaryLog["maxLanes"],
                dictionaryLog["crossingType"],
                dictionaryLog["standardLanes"],
                dictionaryLog["standardLaneTime"],
                dictionaryLog["standardLaneDelay"],
                dictionaryLog["readyLanes"],
                dictionaryLog["readyLanesTime"],
                dictionaryLog["readyLanesDelay"],
                dictionaryLog["fastLanes"],
                dictionaryLog["fastLanesTime"],
                dictionaryLog["fastLanesDelay"],
                dictionaryLog["sentriLanes"],
                dictionaryLog["sentriLanesTime"],
                dictionaryLog["sentriLanesDelay"],
                dictionaryLog["logTime"]
        ]
        
        command = 'INSERT INTO ' + self.tablename + """
        (
            location,
            crossingName,
            maxLanes,
            crossingType,
            standardLanes,
            standardLaneTime,
            standardLaneDelay,
            readyLanes,
            readyLanesTime,
            readyLanesDelay,
            fastLanes,
            fastLanesTime,
            fastLanesDelay,
            sentriLanes,
            sentriLanesTime,
            sentriLanesDelay,
            logTime
        ) 
        VALUES 
        (
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?
        )
        """

        self.c.execute(command, params)
        self.conn.commit()

    def getAll(self):
        crossingTypes = ["Commercial", "Passenger", "Pedestrian"]
        crossingLanes = ["standardLanes", "readyLanes", "fastLanes", "sentriLanes"]
        crossingLaneTimes = [i + "Time" for i in crossingLanes]
        crossingLaneDelay = [i + "Delay" for i in crossingLanes]
        cmd = "SELECT DISTINCT crossingName FROM " + self.tablename#+ " WHERE crossingName=\"{}\"".format(x)
        print(cmd)
        self.c.execute(cmd)
        crossingNames = self.c.fetchall()
        res = crossingNames
        print(res)
        print(type(res))
        print(len(res))

        print(crossingLaneTimes)
        print(crossingLaneDelay)

        returnDict = {}

        for c in crossingNames:
            for t in crossingTypes:
                for l in crossingLanes:
                    d = self.getLocation(c[0],t, l)
                    if d == None:
                        pass
                    elif d[0] == "closed":
                        returnDict["crossingName"] = c[0]
                        returnDict["time"] = d[1]
                        returnDict["crossingType"] = t
                        returnDict["laneType"] = l
                        returnDict["numLanes"] = d[0]
                        returnDict["delay"] = d[2]
                        print("{crossing} at {time}: {crossingType} {laneType} is closed"
                        .format(crossing=c[0], time=d[1], crossingType=t, laneType=l,delay=d[2]))

                    else:
                        returnDict["crossingName"] = c[0]
                        returnDict["time"] = d[1]
                        returnDict["crossingType"] = t
                        returnDict["laneType"] = l
                        returnDict["numLanes"] = d[0]
                        returnDict["delay"] = d[2]
                        print(returnDict)
                        print("{crossing} at {time}:Delay is {delay}: {numLanes} {crossingType} {laneType} open"
                        .format(crossing=c[0], time=d[1], numLanes=d[0], crossingType=t, laneType=l,delay=d[2]))

    
    def getLocation(self, cName, cType, lType):
        # print(cName, cType, lType)
        if lType == "standardLanes":
            tempType = "standardLane"
        else:
            tempType = lType
        
        updateTime = tempType + "Time"
        delay = tempType + "Delay"

        command = """
        SELECT {l}, {u}, {t} FROM {tb}
        WHERE crossingName="{cN}"
        AND crossingType="{cT}"
        AND {l} != "NULL"
        AND {u} != "NULL"
        AND {t} != "NULL"
        AND {u} != "closed"
        """.format(l=lType, u=updateTime, t=delay, tb=self.tablename, cN=cName, cT=cType)
        
        self.c.execute(command)
        res = self.c.fetchall()
        if len(res) > 0:
            # print(command)
            # print(res)
            mostRecent = sorted(res, key=itemgetter(1),reverse=True)[0]
            # print(mostRecent)
            return mostRecent
        else:
            return None
        # panFrame = pd.read_sql_query(command, self.conn)
        #return lanes open, lane time, lane delay

        pass






