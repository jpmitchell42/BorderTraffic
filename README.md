# Border Traffic Webscraper -> SQLite DB

This sets up a web scraper to scrape the US Customs and Border Protection Websites for wait times at crossings of the US/Mexico border and logs it into an SQLite database. 

## Getting Started
```
git clone https://github.com/jpmitchell42/BorderTraffic.git

python3 virtualenv -m virtualenv borderEnv

source borderEnv/bin/activate

pip3 install -r requirements.txt

python3 setup.py
```

#### Run the following for one download 
`python3 manage.py`

#### Otherwise set up in crontab (below is set for every 10 minutes)
`chmod +x pullLogs`

`crontab -e`

Add in the editor:
`*/10 * * * * /PATH/TO/THISFILE/pullLogs`


### Prerequisites

virtualenv

pip3


## Log Info

Logs are by default placed in a database called "crossings.db" in a table called waitlogs

The logs contain the data sorted by location, the sub-crossing name. They then contain info about the number of different lanes (Sentri, ready, standard) available for crossing as well as the time the CBP posted the info, the wait-time and the time the log was pulled. 

NULL value exist for crossings that do not have that crossing. (i.e Sentri lanes for a pedestrian crossing or vehicles for commerical/passenger data at San Ysidro PedWest). Closed means it was listed as closed. 

### Example Logs
|location|	crossingName|	maxLanes|	crossingType|	standardLanes|	standardLaneTime|	standardLaneDelay|	readyLanes|	readyLanesTime|	readyLanesDelay|	fastLanes|	fastLanesTime|	fastLanesDelay|	sentriLanes|	sentriLanesTime|	sentriLanesDelay|	logTime|
|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
Douglas (Raul Hector Castro)|	Douglas (Raul Hector Castro)|	7|	Passenger|	2|	3/4/19 20:00|	25|	3|	3/4/19 20:00|	15|	NULL|	NULL|	NULL|	1|	3/4/19 20:00|	0|	3/4/19 19:09|
Eagle Pass|	Eagle Pass - Bridge I|	5|	Passenger|	1|	3/4/19 21:00|	60|	3|	3/4/19 21:00|	60|	NULL|	NULL|	NULL|	NULL|	NULL|	NULL|	3/4/19 19:09|

## Next steps
  * Create Python Library of queries to the database
  * Create API and host to allow querying for developers
  * Host website with django
  * Analyze data


## Contributing

Contact me to contribute at james.p.mitchell5@gmail.com

## Authors

* **James Mitchell** - https://github.com/jpmitchell42


