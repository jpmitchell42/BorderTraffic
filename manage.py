from cbp import simple_get
from bs4 import BeautifulSoup
import re
from crossing import Crossing
from crossing import SuperCrossing
import urllib.request


ysidrio = 'https://apps.cbp.gov/bwt/mobile.asp?action=n&pn=2504'
hidalgo = 'https://apps.cbp.gov/bwt/mobile.asp?action=n&pn=2305'
elpaso = 'https://apps.cbp.gov/bwt/mobile.asp?action=n&pn=2402'
abay = 'https://apps.cbp.gov/bwt/mobile.asp?action=n&pn=0708'
brownsville = 'https://apps.cbp.gov/bwt/mobile.asp?action=n&pn=5355'






###Main Program starts here. 

#scrape all links and put in array. 
# resp = urllib.request.urlopen("https://apps.cbp.gov/bwt/mobile.asp?action=n&pn=2504")
# soup = BeautifulSoup(resp, from_encoding=resp.info().get_param('charset'),features="html.parser")
# links = []
# for link in soup.find_all('a', href=True):
#     # print(link['href'])
#     if "pn" in link['href']:
#         links.append("https://apps.cbp.gov/bwt/mobile.asp?action=n&pn=" + str(link['href'])[-4:])
# for l in links:
#     Crossing.getMainSubCrossings(l)
#     pass


#for testing, the El Paso crossing is great
# Crossing.getMainSubCrossings(brownsville)




def __main__():
    resp = urllib.request.urlopen("https://apps.cbp.gov/bwt/mobile.asp?action=n&pn=2504")
    soup = BeautifulSoup(resp, from_encoding=resp.info().get_param('charset'),features="html.parser")
    links = []
    for link in soup.find_all('a', href=True):
        # print(link['href'])
        if "pn" in link['href']:
            links.append("https://apps.cbp.gov/bwt/mobile.asp?action=n&pn=" + str(link['href'])[-4:])
    for l in links:
        Crossing.getMainSubCrossings(l)
    
__main__()